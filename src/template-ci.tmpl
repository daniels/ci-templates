# vim: set expandtab shiftwidth=2 tabstop=8 textwidth=0:

variables:
  {{PACKAGES}}: {% if curl -%}
  'wget'
  {% else -%}
  'curl'
  {% endif %}
  {{DISTRIBUTION}}_EXEC: '{{ script }}'


################################################################################
#
# {{distribution.title()}} checks
#
################################################################################


#
# A few templates to avoid writing the image and stage in each job
#
.{{distribution}}:ci@container-build:
  extends: .{{distribution}}@container-build
  image: $CI_REGISTRY_IMAGE/buildah:$BOOTSTRAP_TAG
  stage: {{distribution}}_container_build
  needs:
    - bootstrap
    - sanity check


.{{distribution}}:ci@container-ifnot-exists:
  extends: .{{distribution}}@container-ifnot-exists
  image: $CI_REGISTRY_IMAGE/buildah:$BOOTSTRAP_TAG
  stage: {{distribution}}_container_build
  needs:
    - bootstrap
    - sanity check

{% if aarch64 %}

.{{distribution}}:ci@container-build@arm64v8:
  extends: .{{distribution}}@container-build@arm64v8
  image: $CI_REGISTRY_IMAGE/arm64v8/buildah:$BOOTSTRAP_TAG
  stage: {{distribution}}_container_build
  needs:
    - bootstrap@arm64v8
    - sanity check


.{{distribution}}:ci@container-ifnot-exists@arm64v8:
  extends: .{{distribution}}@container-ifnot-exists@arm64v8
  image: $CI_REGISTRY_IMAGE/arm64v8/buildah:$BOOTSTRAP_TAG
  stage: {{distribution}}_container_build
  needs:
    - bootstrap@arm64v8
    - sanity check
{% endif %}
{% if qemu %}

#
# Qemu build
#
.{{distribution}}:ci@qemu-build:
  extends: .{{distribution}}@qemu-build
  image: $CI_REGISTRY_IMAGE/fedora/qemu-mkosi-base:$QEMU_TAG
  stage: {{distribution}}_container_build
  artifacts:
    name: logs-$CI_PIPELINE_ID
    when: always
    expire_in: 1 week
    paths:
      - console.out
  needs:
    - bootstrap-qemu-mkosi
    - sanity check
{% endif %}

#
# generic {{distribution}} checks
#
.{{distribution}}@check:
  stage: {{distribution}}_check
  script:
{% if not curl %}
      # make sure curl has been installed
    - curl --insecure https://gitlab.freedesktop.org
{% else %}
      # make sure wget has been installed
    - wget https://gitlab.freedesktop.org
{% endif %}
      # make sure our test script has been run
    - if [[ -e /test_file ]] ;
      then
        echo ${{DISTRIBUTION}}_EXEC properly run ;
      else
        exit 1 ;
      fi
{% if qemu %}


.{{distribution}}@qemu-check:
  stage: {{distribution}}_check
  tags:
    - kvm
  script:
    - pushd /app
      # start the VM
    - bash /app/start_vm.sh
{% if not curl %}
      # make sure curl has been installed
    - ssh -p 5555 localhost curl --insecure https://gitlab.freedesktop.org
{% else %}
      # make sure wget has been installed
    - ssh -p 5555 localhost wget https://gitlab.freedesktop.org
{% endif %}
      # terminate the VM
    - ssh -p 5555 localhost halt -p || true
    - sleep 2
    - kill $(pgrep qemu) || true

      # start the VM, with the kernel parameters
    - bash /app/start_vm_kernel.sh
{% if not curl %}
      # make sure we can still use curl
    - ssh -p 5555 localhost curl --insecure https://gitlab.freedesktop.org
{% else %}
      # make sure we can still use wget
    - ssh -p 5555 localhost wget https://gitlab.freedesktop.org
{% endif %}
      # terminate the VM
    - ssh -p 5555 localhost halt -p || true
    - sleep 2
    - kill $(pgrep qemu) || true
  artifacts:
    name: logs-$CI_PIPELINE_ID
    when: always
    expire_in: 1 week
    paths:
      - console.out
{% endif %}


{# We only do the full test on the first version of the distribution #}
{# other versions have a slighter check at the end #}
#
# straight {{distribution}} build and test
#
{{distribution}}:{{ versions[0] }}@container-build:
  extends: .{{distribution}}:ci@container-build
  variables:
{% if not version %}
    {{VERSION.strip('$')}}: '{{ versions[0] }}'
{% endif %}
    {{DISTRIBUTION}}_TAG: $CI_PIPELINE_ID


{{distribution}}:{{ versions[0] }}@check:
  extends: .{{distribution}}@check
  image: $CI_REGISTRY_IMAGE/{{docker_repo}}/{{ versions[0] }}:$CI_PIPELINE_ID
  needs:
    - {{distribution}}:{{ versions[0] }}@container-build
    - sanity check
{% if aarch64 %}


{{distribution}}:{{ versions[0] }}@container-build@arm64v8:
  extends: .{{distribution}}:ci@container-build@arm64v8
  variables:
{% if not version %}
    {{VERSION.strip('$')}}: '{{ versions[0] }}'
{% endif %}
    {{DISTRIBUTION}}_TAG: arm64v8-$CI_PIPELINE_ID


{{distribution}}:{{ versions[0] }}@check@arm64v8:
  extends: .{{distribution}}@check
  image: $CI_REGISTRY_IMAGE/{{docker_repo}}/{{ versions[0] }}:arm64v8-$CI_PIPELINE_ID
  tags:
    - aarch64
  needs:
    - {{distribution}}:{{ versions[0] }}@container-build@arm64v8
    - sanity check
{% endif %}
{% if qemu %}


{{distribution}}:{{ versions[0] }}@qemu-build:
  extends: .{{distribution}}:ci@qemu-build
  variables:
{% if not version %}
    {{VERSION.strip('$')}}: '{{ versions[0] }}'
{% endif %}
    {{DISTRIBUTION}}_TAG: qemu-$CI_PIPELINE_ID
    {{ PACKAGES }}: 'wget curl'
    QEMU_BASE_IMAGE: $CI_REGISTRY_IMAGE/fedora/qemu-base:$QEMU_TAG


{{distribution}}:{{ versions[0] }}@qemu-check:
  extends: .{{distribution}}@qemu-check
  image: $CI_REGISTRY_IMAGE/{{docker_repo}}/{{ versions[0] }}:qemu-$CI_PIPELINE_ID
  needs:
    - {{distribution}}:{{ versions[0] }}@qemu-build
    - sanity check
{% endif %}


#
# make sure we do rebuild the image if the tag does not exist and check
#
{{distribution}}-forced:{{ versions[0] }}@container-ifnot-exists:
  extends: .{{distribution}}:ci@container-ifnot-exists
  variables:
    UPSTREAM_REPO: $CI_PROJECT_PATH
{% if not version %}
    {{VERSION.strip('$')}}: '{{ versions[0] }}'
{% endif %}
    {{DISTRIBUTION}}_TAG: $CI_PIPELINE_IID


{{distribution}}-forced-ifnot-exists:{{ versions[0] }}@check:
  extends: .{{distribution}}@check
  image: $CI_REGISTRY_IMAGE/{{docker_repo}}/{{ versions[0] }}:$CI_PIPELINE_IID
  needs:
    - {{distribution}}-forced:{{ versions[0] }}@container-ifnot-exists
    - sanity check


#
# make sure we do not rebuild the image if the tag exists (during the check)
#
{{distribution}}:{{ versions[0] }}@container-ifnot-exists:
  extends: .{{distribution}}:ci@container-ifnot-exists
  stage: {{distribution}}_check
  variables:
    UPSTREAM_REPO: $CI_PROJECT_PATH
{% if not version %}
    {{VERSION.strip('$')}}: '{{ versions[0] }}'
{% endif %}
    {{DISTRIBUTION}}_TAG: $CI_PIPELINE_IID
    {{ PACKAGES }}: 'this-package-should-not-exist'
  needs:
    - {{distribution}}-forced:{{ versions[0] }}@container-ifnot-exists
    - sanity check


#
# make sure we do not rebuild the image if the tag exists in the upstream
# repository (during the check)
# special case where REPO_SUFFIX == ci_templates_test_upstream
#
{{distribution}}:{{ versions[0] }}-upstream@container-ifnot-exists:
  extends: .{{distribution}}:ci@container-ifnot-exists
  stage: {{distribution}}_check
  variables:
    UPSTREAM_REPO: $CI_PROJECT_PATH
    REPO_SUFFIX: ci_templates_test_upstream
{% if not version %}
    {{VERSION.strip('$')}}: '{{ versions[0] }}'
{% endif %}
    {{DISTRIBUTION}}_TAG: $CI_PIPELINE_IID
    {{ PACKAGES }}: 'this-package-should-not-exist'
  needs:
    - {{distribution}}-forced:{{ versions[0] }}@container-ifnot-exists
    - sanity check
{% if versions[1:] %}

#
# Try our {{distribution}} scripts with other versions and check
#
{% for v in versions[1:] %}

{{distribution}}:{{v}}@container-build:
  extends: .{{distribution}}:ci@container-build
  variables:
    {{VERSION.strip('$')}}: '{{v}}'
    {{DISTRIBUTION}}_TAG: $CI_PIPELINE_ID

{{distribution}}:{{v}}@check:
  extends: .{{distribution}}@check
  image: $CI_REGISTRY_IMAGE/{{docker_repo}}/{{v}}:$CI_PIPELINE_ID
  needs:
    - {{distribution}}:{{v}}@container-build
    - sanity check
{% endfor %}
{% endif %}

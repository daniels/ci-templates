# vim: set expandtab shiftwidth=2 tabstop=8 textwidth=0:

variables:
  FEDORA_RPMS: 'wget'
  FEDORA_EXEC: '.hidden_dir/test.sh /test_file'


################################################################################
#
# Fedora checks
#
################################################################################


#
# A few templates to avoid writing the image and stage in each job
#
.fedora:ci@container-build:
  extends: .fedora@container-build
  image: $CI_REGISTRY_IMAGE/buildah:$BOOTSTRAP_TAG
  stage: fedora_container_build
  needs:
    - bootstrap
    - sanity check


.fedora:ci@container-ifnot-exists:
  extends: .fedora@container-ifnot-exists
  image: $CI_REGISTRY_IMAGE/buildah:$BOOTSTRAP_TAG
  stage: fedora_container_build
  needs:
    - bootstrap
    - sanity check


.fedora:ci@container-build@arm64v8:
  extends: .fedora@container-build@arm64v8
  image: $CI_REGISTRY_IMAGE/arm64v8/buildah:$BOOTSTRAP_TAG
  stage: fedora_container_build
  needs:
    - bootstrap@arm64v8
    - sanity check


.fedora:ci@container-ifnot-exists@arm64v8:
  extends: .fedora@container-ifnot-exists@arm64v8
  image: $CI_REGISTRY_IMAGE/arm64v8/buildah:$BOOTSTRAP_TAG
  stage: fedora_container_build
  needs:
    - bootstrap@arm64v8
    - sanity check

#
# Qemu build
#
.fedora:ci@qemu-build:
  extends: .fedora@qemu-build
  image: $CI_REGISTRY_IMAGE/fedora/qemu-mkosi-base:$QEMU_TAG
  stage: fedora_container_build
  artifacts:
    name: logs-$CI_PIPELINE_ID
    when: always
    expire_in: 1 week
    paths:
      - console.out
  needs:
    - bootstrap-qemu-mkosi
    - sanity check

#
# generic fedora checks
#
.fedora@check:
  stage: fedora_check
  script:
      # make sure wget has been installed
    - wget https://gitlab.freedesktop.org
      # make sure our test script has been run
    - if [[ -e /test_file ]] ;
      then
        echo $FEDORA_EXEC properly run ;
      else
        exit 1 ;
      fi


.fedora@qemu-check:
  stage: fedora_check
  tags:
    - kvm
  script:
    - pushd /app
      # start the VM
    - bash /app/start_vm.sh
      # make sure wget has been installed
    - ssh -p 5555 localhost wget https://gitlab.freedesktop.org
      # terminate the VM
    - ssh -p 5555 localhost halt -p || true
    - sleep 2
    - kill $(pgrep qemu) || true

      # start the VM, with the kernel parameters
    - bash /app/start_vm_kernel.sh
      # make sure we can still use wget
    - ssh -p 5555 localhost wget https://gitlab.freedesktop.org
      # terminate the VM
    - ssh -p 5555 localhost halt -p || true
    - sleep 2
    - kill $(pgrep qemu) || true
  artifacts:
    name: logs-$CI_PIPELINE_ID
    when: always
    expire_in: 1 week
    paths:
      - console.out


#
# straight fedora build and test
#
fedora:31@container-build:
  extends: .fedora:ci@container-build
  variables:
    FEDORA_VERSION: '31'
    FEDORA_TAG: $CI_PIPELINE_ID


fedora:31@check:
  extends: .fedora@check
  image: $CI_REGISTRY_IMAGE/fedora/31:$CI_PIPELINE_ID
  needs:
    - fedora:31@container-build
    - sanity check


fedora:31@container-build@arm64v8:
  extends: .fedora:ci@container-build@arm64v8
  variables:
    FEDORA_VERSION: '31'
    FEDORA_TAG: arm64v8-$CI_PIPELINE_ID


fedora:31@check@arm64v8:
  extends: .fedora@check
  image: $CI_REGISTRY_IMAGE/fedora/31:arm64v8-$CI_PIPELINE_ID
  tags:
    - aarch64
  needs:
    - fedora:31@container-build@arm64v8
    - sanity check


fedora:31@qemu-build:
  extends: .fedora:ci@qemu-build
  variables:
    FEDORA_VERSION: '31'
    FEDORA_TAG: qemu-$CI_PIPELINE_ID
    FEDORA_RPMS: 'wget curl'
    QEMU_BASE_IMAGE: $CI_REGISTRY_IMAGE/fedora/qemu-base:$QEMU_TAG


fedora:31@qemu-check:
  extends: .fedora@qemu-check
  image: $CI_REGISTRY_IMAGE/fedora/31:qemu-$CI_PIPELINE_ID
  needs:
    - fedora:31@qemu-build
    - sanity check


#
# make sure we do rebuild the image if the tag does not exist and check
#
fedora-forced:31@container-ifnot-exists:
  extends: .fedora:ci@container-ifnot-exists
  variables:
    UPSTREAM_REPO: $CI_PROJECT_PATH
    FEDORA_VERSION: '31'
    FEDORA_TAG: $CI_PIPELINE_IID


fedora-forced-ifnot-exists:31@check:
  extends: .fedora@check
  image: $CI_REGISTRY_IMAGE/fedora/31:$CI_PIPELINE_IID
  needs:
    - fedora-forced:31@container-ifnot-exists
    - sanity check


#
# make sure we do not rebuild the image if the tag exists (during the check)
#
fedora:31@container-ifnot-exists:
  extends: .fedora:ci@container-ifnot-exists
  stage: fedora_check
  variables:
    UPSTREAM_REPO: $CI_PROJECT_PATH
    FEDORA_VERSION: '31'
    FEDORA_TAG: $CI_PIPELINE_IID
    FEDORA_RPMS: 'this-package-should-not-exist'
  needs:
    - fedora-forced:31@container-ifnot-exists
    - sanity check


#
# make sure we do not rebuild the image if the tag exists in the upstream
# repository (during the check)
# special case where REPO_SUFFIX == ci_templates_test_upstream
#
fedora:31-upstream@container-ifnot-exists:
  extends: .fedora:ci@container-ifnot-exists
  stage: fedora_check
  variables:
    UPSTREAM_REPO: $CI_PROJECT_PATH
    REPO_SUFFIX: ci_templates_test_upstream
    FEDORA_VERSION: '31'
    FEDORA_TAG: $CI_PIPELINE_IID
    FEDORA_RPMS: 'this-package-should-not-exist'
  needs:
    - fedora-forced:31@container-ifnot-exists
    - sanity check

#
# Try our fedora scripts with other versions and check
#

fedora:30@container-build:
  extends: .fedora:ci@container-build
  variables:
    FEDORA_VERSION: '30'
    FEDORA_TAG: $CI_PIPELINE_ID

fedora:30@check:
  extends: .fedora@check
  image: $CI_REGISTRY_IMAGE/fedora/30:$CI_PIPELINE_ID
  needs:
    - fedora:30@container-build
    - sanity check

fedora:rawhide@container-build:
  extends: .fedora:ci@container-build
  variables:
    FEDORA_VERSION: 'rawhide'
    FEDORA_TAG: $CI_PIPELINE_ID

fedora:rawhide@check:
  extends: .fedora@check
  image: $CI_REGISTRY_IMAGE/fedora/rawhide:$CI_PIPELINE_ID
  needs:
    - fedora:rawhide@container-build
    - sanity check
